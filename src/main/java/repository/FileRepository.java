package repository;

public interface FileRepository {
	
	String read (String path);
	
	void write (String content, String file_name);

}
