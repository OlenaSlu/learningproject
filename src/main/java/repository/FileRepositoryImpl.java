package repository;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileRepositoryImpl implements FileRepository {

	@Override
	public String read(String path) {

		try {
			FileReader fr = new FileReader(path);
			Scanner scan = new Scanner(fr);
			while (scan.hasNextLine()) {
				System.out.println(scan.nextLine());
			}
			scan.close();
			fr.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return "//////////";
	}

	@Override
	public void write(String content, String fileName) {

		File fl = new File(fileName);
		try (PrintWriter pw = new PrintWriter(fl)) {

			pw.write(content);

		} catch (IOException e) {
			System.out.println(e);

		}
	}
}
