package customers;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;

import dto.Customer;
import repository.FileRepository;
import repository.FileRepositoryImpl;
import service.CustomerJsonServiceImpl;
import service.CustomerService;
import service.CustomerXmlServiceImpl;



public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
	 
		ArrayList<Customer> list1 = new ArrayList<Customer>();
		
		
		Customer c1 = new Customer("Sasha", "Petrenko", new Date(365542187000L),new Address("Irpin", "Djerelna", 12345));
		Customer c2 = new Customer("Oleg", "Protsenko", new Date(585542387000L), new Address("Irpin", "Soborna", 12356));
		Customer c3 = new Customer("Ivan", "Ivanov", new Date(655542387000L), new Address("Borispol", "Myru", 18888));
		Customer c4 = new Customer("Oleg", "Ivanov", new Date(955542387000L), new Address("Borispol", "Myru", 18888));
		Customer c5 = new Customer("Inna", "Dub", new Date(555902387000L), new Address("Kiev", "Kiltseva", 18890));
		Customer c6 = new Customer("Olena", "Slushna", new Date(557842387000L), new Address("Irpin", "Djerelna", 18867));
		Customer c7 = new Customer("Leonid", "Datsenko", new Date(577542387000L), new Address("Irpin", "Djerelna", 18867));
		Customer c8 = new Customer("Marina", "Marininaya", new Date(595542387000L), new Address("Bakhmach", "Lomonosova", 16500));

		

		list1.add(c1);
		list1.add(c2);
		list1.add(c3);
		list1.add(c4);
		list1.add(c5);
		list1.add(c6);
		list1.add(c7);
		list1.add(c8);

		CustomerService wr = new CustomerJsonServiceImpl(new FileRepositoryImpl());
		wr.write(list1);

		CustomerService rd = new CustomerJsonServiceImpl(new FileRepositoryImpl());
		rd.read();
		
		

		CustomerService wr2 = new CustomerXmlServiceImpl();
		wr2.write(list1);

		CustomerService rd2 = new CustomerXmlServiceImpl();
		rd2.read();

	
		
	}
}
