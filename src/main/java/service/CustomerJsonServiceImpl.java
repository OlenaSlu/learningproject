package service;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import dto.Customer;
import repository.FileRepository;

public class CustomerJsonServiceImpl implements CustomerService {

	private final FileRepository repository;

	public CustomerJsonServiceImpl(FileRepository repository) {
		super();
		this.repository = repository;
		
	}
	
//	FileRepository rep = new FileRepositoryImpl();


	
	@Override
	public void write(List<Customer> a) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String gsonS = gson.toJson(a);

		repository.write(gsonS, "file_Json.json");

	}

	@Override
	public List<Customer> read() {

		Gson gson = new Gson();

		ArrayList<Customer> s = gson.fromJson(repository.read("file_Json.json"), new TypeToken<ArrayList<Customer>>() {
		}.getType());

		return s;

	}

}
