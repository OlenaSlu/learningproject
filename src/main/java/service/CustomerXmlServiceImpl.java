package service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import dto.Customer;
import dto.CustomerList;
import repository.FileRepository;
import repository.FileRepositoryImpl;

public class CustomerXmlServiceImpl implements CustomerService {

	@Override
	public List<Customer> read() {

		try {

			FileInputStream fis = new FileInputStream("file_Xml.xml");
			JAXBContext context = JAXBContext.newInstance(CustomerList.class);
			Unmarshaller unmurshaller = context.createUnmarshaller();
			CustomerList customerList = (CustomerList) unmurshaller.unmarshal(fis);

			System.out.println(customerList.getData());

			return customerList.getData();

		} catch (Exception e) {

			e.printStackTrace();
		}
		return new ArrayList<>();

	}

	@Override
	public void write(List<Customer> data) {

		CustomerList customers = new CustomerList();
		customers.setData(data);
		FileRepository rep = new FileRepositoryImpl();

		try {

			ByteArrayOutputStream os = new ByteArrayOutputStream();

			JAXBContext jaxbC = JAXBContext.newInstance(CustomerList.class);
			Marshaller marSh = jaxbC.createMarshaller();
			marSh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marSh.marshal(customers, os);

			String finalString = new String(os.toByteArray());
			rep.write(finalString, "file_Xml.xml");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
