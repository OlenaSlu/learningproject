package service;

import java.util.List;

import dto.Customer;

public interface CustomerService {

	public void write(List<Customer> a);

	public List<Customer> read();

}
