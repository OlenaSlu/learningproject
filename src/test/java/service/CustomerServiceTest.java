package service;

import java.util.ArrayList;
import java.util.Date;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;
import org.junit.Test;
import customers.Address;
import dto.Customer;
import repository.FileRepository;

public class CustomerServiceTest {
	
    

	@Test
	public void test_read_Json1() {

		FileRepository repositoryMock = mock(FileRepository.class);

		CustomerJsonServiceImpl jsonService = new CustomerJsonServiceImpl(repositoryMock);
		jsonService.read();

		verify(repositoryMock, times(1)).read("file_Json.json");
	}

	@Test
	public void test_read_Json2() {

		FileRepository repositoryMock = mock(FileRepository.class);
		
		CustomerJsonServiceImpl jsonService = new CustomerJsonServiceImpl(repositoryMock);
		jsonService.read();
		
	
		when(repositoryMock.read("file_Json.json")).thenReturn("\"file_Json.json\"");
		

		assertEquals("\"file_Json.json\"",jsonService.read());
		



	}

}
